// https://www.yuque.com/antv/g6-editor/flow-api

const data = {
    nodes: [{
        id: 'node1',
        label: {
            text: 'node 1',
            fill: 'blue'
        },
        x: 100,
        y: 200,
        style: {
            fill: '#3333',
            stroke: 'red'
        }
    },{
        id: 'node2',
        label: {
            text: 'node 2'
        },
        x: 300,
        y: 200
    }],
    edges: [{
        target: 'node2',
        source: 'node1'
    }]
};

const editor = new G6Editor();
const page = new G6Editor.Flow({
    graph: {
        container: 'page',
    },
});
const toolbar = new G6Editor.Toolbar({
    container: 'toolbar',
});
const itempannel = new G6Editor.Itempannel({
    container: 'itempannel',
});
const detailpannel = new G6Editor.Detailpannel({
    container: 'detailpannel',
});

editor.add(page);
editor.add(toolbar);
editor.add(itempannel);
editor.add(detailpannel);

page.read(data);
window.flow = page;
